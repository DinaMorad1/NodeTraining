var mongoose = require('mongoose');
var $ = require("mongoose/lib/statemachine.js");
const Schema = mongoose.Schema;
var User = require('../models/user');
// var mongoosePaginate = require('mongoose-paginate');


var bookSchema = new mongoose.Schema({
    title: {type: String, required: [true, 'Title can\'t be empty.']},
    description: {type: String, required: [true, 'Description can\'t be empty.']},
    comments: [{
        body: {type: String, default: ''},
        user: {type: Schema.ObjectId, ref: 'users'},
        createdAt: {type: Date, default: Date.now}
    }],
    users: [{type: Schema.ObjectId, ref: 'users'}],
    copies_num: {type: Number, default: 0},
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
});
// bookSchema.plugin(mongoosePaginate);
mongoose.model('books', bookSchema);

var commentUserPopulate = 'comments.user';
var usersPopulate = 'users';

exports.list = function (per, page, searchQuery, result) {
    //decrementing page starting index.
    page--;
    var findQuery = searchQuery ? mongoose.model('books').find({title: {$regex: searchQuery}}) : mongoose.model('books').find();

    findQuery
        .populate(commentUserPopulate)
        .populate(usersPopulate)
        .limit(per)
        .skip(per * page)
        .exec(function (err, books) {
            if (!err)
                result(books);
        });

    //couldn't use this library because of search and populate.
    // mongoose.model('books').paginate({}, {page: page, limit: per}, function (err, books) {
    //     if (!err)
    //         result(books);
    // });
}

exports.create = function (body, result) {
    var data = {
        title: body.title,
        description: body.description,
        copies_num: body.copies_num
    };
    mongoose.model('books').create(data, function (err, book) {
        if (err)
            result(null, err);
        else
            result(book, null);
    });
}

exports.get = function (id, result) {
    mongoose.model('books').findById(id, function (err, book) {
        if (!err)
            result(book);
    }).populate(commentUserPopulate).populate(usersPopulate);
}

exports.update = function (id, body, result) {
    var data = {
        title: body.title,
        description: body.description,
        copies_num: body.copies_num
    };
    mongoose.model('books').findById(id, function (err, book) {
        book.update(data,{runValidators: true}, function (err, res) {
            if (!err)
                result(res, null);
            else
                result(book, err);
        });
    }).populate(commentUserPopulate).populate(usersPopulate);
}

exports.rent = function (book, user, result) {
    var bookUsers = book.users;
    if (!bookUsers)
        bookUsers = [];
    book.users.push(user);
    book.copies_num--;
    book.save();

    User.findOne({'_id': user._id}, function (err, user) {
        user.books.push(book);
        user.save();
        result(book);
    });
}

exports.return = function (book, user, result) {
    var position = book.users.map(function (e) {
        return e._id.toString();
    }).indexOf(user._id);

    if (position != -1) {
        book.users.splice(position, 1);
        book.copies_num++;
        book.save();
    }

    User.findOne({'_id': user._id}, function (err, user) {
        var position = user.books.map(function (e) {
            return e._id.toString();
        }).indexOf(book._id.toString());

        if (position != -1) {
            user.books.splice(position, 1);
            user.save();
        }
        result(book);
    }).populate('books');

}


exports.delete = function (id, result) {
    mongoose.model('books').findById(id, function (err, book) {
        if (!err) {
            book.remove(function (err, res) {
                if (!err)
                    result(res);
            });
        }
    });
}

exports.addComment = function (user, book, commentBody, result) {
    var comment = {
        body: commentBody,
        user: user
    };
    book.comments.push(comment);
    if (book.save())
        result(book.comments[book.comments.length - 1]);
    else
        result(null);
}

exports.removeComment = function (book, commentId) {

    var position = book.comments.map(function (e) {
        return e._id.toString();
    }).indexOf(commentId);

    if (position != -1) {
        book.comments.splice(position, 1);
        book.save();
    }
}