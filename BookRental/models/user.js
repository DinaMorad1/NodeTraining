var mongoose = require('mongoose');
const Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
    name: String,
    email: String,
    password: String,
    books: [{type: Schema.ObjectId, ref: 'books'}],
    // books: [{ type :  mongoose.Types.ObjectId()}],//{type: Array, default: []},
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }
});

// methods ======================
// generating a hash for encrypting password's users
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// Return 0 si son iguales y otro valor en cualquier otro caso.
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('users', userSchema);

exports.getById = function (id, result) {
    mongoose.model('users').findById(id, function (err, user) {
        result(user);
    });
}