var express = require('express');
var router = express.Router();
var books = require('../models/book');
var isLoggedIn = require('../Config/auth');
var bookList = [];

/* GET books listing. */
router.route('/')
    .get(isLoggedIn, function (req, res, next) {
        var searchStr = req.query.searchQuery ? req.query.searchQuery : null;
        var renderView = searchStr ? false : true;
        renderBooksList(req, res, renderView);
    })
    .post(isLoggedIn, function (req, res) {
        books.create(req.body, function (book, error) {
            if (book)
                redirect_home(res);
            else {
                res.render('books/create', getCreateBookResponseOnj(error));
            }
        });
    });

router.get('/paginate', isLoggedIn, function (req, result, next) {
    renderBooksList(req, result, false);
});

router.get('/create', isLoggedIn, function (req, res) {
    res.render('books/create', getCreateBookResponseOnj(null));
});

router.get('/edit/:id', isLoggedIn, function (req, res) {
    var id = req.params.id;

    books.get(id, function (result) {
        if (result)
            res.render('books/edit', {data: result, errors: null});
    });
});

router.post('/edit/:id', isLoggedIn, function (req, res) {
    var id = req.params.id;

    books.update(id, req.body, function (result, error) {
        if (error)
            res.render('books/edit', {data: result, errors: error.errors});
        else
            redirect_home(res);
    });
});

router.get('/delete/:id', isLoggedIn, function (req, res) {
    var id = req.params.id;
    books.delete(id, function (result) {
        if (result)
            redirect_home(res);
    });
});

function redirect_home(res) {
    res.redirect('/books?currentPage=1&per=5');
}

function getBooksResponseObj(rows, per, currentPage, req) {
    return {
        data: rows,
        per: per,
        currentPage: currentPage,
        loginUserId: req.user._id
    };
}

function renderBooksList(req, res, renderView) {
    var currentPage = (req && req.query && req.query.currentPage) ? parseInt(req.query.currentPage, 10) : 1;//page num
    var per = req.query.per ? parseInt(req.query.per, 10) : 5;//num of requested items per page
    var searchStr = req.query.searchQuery ? req.query.searchQuery : null;
    books.list(per, currentPage, searchStr, function (rows) {
        bookList.push(rows);
        var responseObj = getBooksResponseObj(rows, per, currentPage, req);
        if (renderView)
            res.render('books/index', responseObj);
        else
            res.send(responseObj);
    });
}

function getCreateBookResponseOnj(err) {
    var responseObj = {
        errors: err ? err.errors : null
    };
    return responseObj;
}

module.exports = router;
