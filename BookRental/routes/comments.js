var express = require('express');
var router = express.Router();
var isLoggedIn = require('../Config/auth');
var books = require('../models/book');
var dateFormat = require('dateformat');
var bookId;

router.get('/:bookId', isLoggedIn, function (req, res) {
    bookId = req.params.bookId;

    books.get(bookId, function (book) {
        if (book) {
            var comments = [];
            var dateFormat = require('dateformat');

            for (var i = 0; i < book.comments.length; i++) {
                comments.push(fillCommentContent(book.comments[i]));
            }
            var sent = JSON.stringify(comments);
            res.render('comments/create', {bookId: bookId, comments: sent});
        }
    });
});

function fillCommentContent(comment) {
    var createdAt = comment.createdAt;
    var date = dateFormat(createdAt, "yyyy-mm-dd h:MM:ss");
    return {
        body: comment.body,
        createdAt: date,
        user: comment.user,
        _id: comment._id
    };
}

router.post('/create/:bookId', isLoggedIn, function (req, res) {
    var bookId = req.params.bookId;
    var commentText = req.body.text;

    books.get(bookId, function (book) {
        if (book) {
            books.addComment(req.user, book, commentText, function (comment) {
                if (comment) {
                    var content = fillCommentContent(comment);
                    res.send(content);
                }

            });
        }
    });
});

router.post('/delete/:commentId', isLoggedIn, function (req, res) {
    var commentId = req.params.commentId;
    var bookId = req.body.bookId;
    books.get(bookId, function (book) {
        books.removeComment(book, commentId);
        res.send('Ok');
    });
});

module.exports = router;