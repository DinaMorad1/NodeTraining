var express = require('express');
var router = express.Router();
var books = require('../models/book');
var isLoggedIn = require('../Config/auth');

//will make it get for now, needs to be changed to post or put and use ajax to send the request
router.post('/rent/:book_id', isLoggedIn, function (req, res) {
    rental(req, res, true);
});

router.post('/return/:book_id', isLoggedIn, function (req, res) {
    rental(req, res, false);
});

function rental(req, res, isRent) {
    var bookId = req.params.book_id;
    var book = books.get(bookId, function (book) {
        var user = req.user;
        if (isRent) {
            if (book.copies_num > 0) {
                books.rent(book, user, function (book) {
                    renderRentalResponse(res, book);
                });
            } else
                res.send('No enough book copies');
        } else {
            books.return(book, user, function (book) {
                renderRentalResponse(res, book);
            });
        }

    });
}

function renderRentalResponse(res, book) {
    if (book) {
        // res.sendStatus(200);//book rent completed successfully
        res.send({status: 200, bookCopiesNum: book.copies_num});
    }
}

module.exports = router;

