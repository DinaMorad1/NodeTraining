var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

/* User login. */
router.get('/login', isLogin, function (req, res) {
    res.render('users/login', {message: req.flash('loginMessage')});
});

/* User signup. */
router.get('/signup', isLogin, function (req, res) {
    res.render('users/signup', {message: req.flash('signupMessage')});
});

router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/books', // redirect to the secure profile section
    failureRedirect: '/users/login', // redirect back to the signup page if there is an error
    failureFlash: 'Invalid name or password' // allow flash messages
}));


router.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/books',
    failureRedirect: '/users/login', // redirect back to the signup page if there is an error
    failureFlash: 'Invalid name or password' // allow flash messages
}));

// send to facebook to do the authentication
router.get('/auth/facebook', passport.authenticate('facebook', {scope: 'email'}));

// handle the callback after facebook has authenticated the user
router.get('/auth/facebook/callback',
    passport.authenticate('facebook', {
        successRedirect: '/books',
        failureRedirect: '/users/login'
    }));


router.get('/logout', function (req, res) {
    if (req.isAuthenticated()) {
        req.logOut();
        req.session.destroy(function (err) {
            if (!err)
                res.redirect('/users/login');
        });
    }
});

function isLogin(req, res, next) {
    if (req.isAuthenticated())
        res.redirect('/books');
    else
        return next();
}

module.exports = router;
